<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends CI_Model {


#--------------------------------Kriteria------------------------------------

	public function kri_insert($data){
		$insert = $this->db->insert("kriteria", $data);
		return $insert;
	}

	public function kri_update($set, $where){
		$update = $this->db->update("kriteria",$set, $where);
		return $update;
	}

	public function kri_delete($where){
		$delete = $this->db->delete("kriteria", $where);
		return $delete;
	}

	public function kri_get(){
		$data = $this->db->get("kriteria")->result();
		return $data;
	}

	public function kri_get_where($where){
		$data = $this->db->get_where("kriteria", $where)->row_array();
		return $data;
	}

#--------------------------------Sub_Kriteria--------------------------------

	public function sub_kri_insert($data){
		$insert = $this->db->insert("kriteria_sub", $data);
		return $insert;
	}

	public function sub_kri_update($set, $where){
		$update = $this->db->update("kriteria_sub",$set, $where);
		return $update;
	}

	public function sub_kri_delete($where){
		$delete = $this->db->delete("kriteria_sub", $where);
		return $delete;
	}

	public function sub_kri_get(){
		$this->db->join("kriteria k", "k.id_kri=ks.id_kri");
		$data = $this->db->get("kriteria_sub ks")->result();
		return $data;
	}

	public function sub_kri_get_where($where){
		$data = $this->db->get_where("kriteria_sub", $where);
		return $data;
	}

	public function sub_kri_get_wher_asc($where){
		$this->db->order_by("val_sub", "asc");
		$data = $this->db->get_where("kriteria_sub", $where);
		return $data;
	}

	
}
