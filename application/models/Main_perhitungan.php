<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_perhitungan extends CI_Model {


#------------------------------------------------------------------Dosen----------------------------------------------------------------------

	public function perhitungan_insert($data){
		$insert = $this->db->insert_batch("perbandingan_kri", $data);
		return $insert;
	}

	public function perhitungan_get(){
		$data = $this->db->get("perbandingan_kri")->result();
		return $data;
	}

	// public function get_perbandingan_all(){
	// 	$data = $this->db->get("perbandingan_kri")->result();
	// 	return $data;
	// }

	public function perhitungan_delete(){
		$delete = $this->db->empty_table("perbandingan_kri");
		return $delete;
	}

	
	
}
