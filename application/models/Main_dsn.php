<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_dsn extends CI_Model {


#------------------------------------------------------------------Dosen----------------------------------------------------------------------

	public function dsn_insert($data){
		$insert = $this->db->insert("dsn", $data);
		return $insert;
	}

	public function dsn_update($set, $where){
		$update = $this->db->update("dsn",$set, $where);
		return $update;
	}

	public function dsn_delete($where){
		$delete = $this->db->delete("dsn", $where);
		return $delete;
	}

	public function dsn_get(){
		$data = $this->db->get("dsn")->result();
		return $data;
	}

	public function dsn_get_where($where){
		$data = $this->db->get_where("dsn", $where)->row_array();
		return $data;
	}

#------------------------------------------------------------------penilaian----------------------------------------------------------------------
	public function insert_penilaian($data){
		$insert = $this->db->insert("penilaian", $data);
		return $insert;
	}

	public function dell_all_penilaian($where){
		$delete = $this->db->delete("penilaian", $where);
		return $delete;
	}

	public function get_all_penilaian($where){
		$this->db->join("kriteria kr", "p.id_kri=kr.id_kri");
		$this->db->join("kriteria_sub krs", "p.id_sub_kri=krs.id_sub_kri");
		$delete = $this->db->get_where("penilaian p", $where);
		return $delete;
	}

	public function penilaian_update($set, $where){
		$update = $this->db->update("penilaian", $set, $where);
		return $update;
	}
}
