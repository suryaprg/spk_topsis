<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpenilaian extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("kriteria", "kri");
		$this->load->model("main_dsn", "md");
		$this->load->model("main/mainmodel", "mm");

        $this->load->library("response_message");

        if(isset($_SESSION["admin_lv_1"])){
			if($this->session->userdata("admin_lv_1")["is_log"] != 1){
				redirect(base_url()."admin/login");
			}
		}else{
			redirect(base_url()."admin/login");
		}
        
	}

#=================================================================================================#
#-------------------------------------------main_penilaian_dosen----------------------------------#
#=================================================================================================#
	public function index(){
		// $data = $this->mm->get_dsn_fix();
		// print_r($data);
		$data["page"] = "page_penilaian";
		$data["dsn"] = $this->mm->get_data_all("dsn");
		$data["kriteria"] = $this->kri->kri_get();
		foreach ($data["kriteria"] as $val_kriteria) {
			$data["sub_kri"][$val_kriteria->id_kri] = $this->kri->sub_kri_get_where(array("id_kri"=>$val_kriteria->id_kri))->result();
		}

		$dsn = $this->md->dsn_get();

		$data["penilainan_all"] = array();
		foreach ($dsn as $r_dsn => $val_dsn) {
			$data["penilainan_all"][$r_dsn]["dsn"] = $val_dsn;
			$data["penilainan_all"][$r_dsn]["val"] = $this->md->get_all_penilaian(array("nidn"=>$val_dsn->id_dsn))->result();
		}

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("index",$data);
	}

	private function val_penilaian_dsn(){
        $config_val_input = array(
                array(
                    'field'=>'dsn',
                    'label'=>'Nama Dosen',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function insert_penilaian_dsn(){
		$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
		$msg_detail = array("dsn" => "");

		if($this->val_penilaian_dsn()){
			$dsn = $this->input->post("dsn");
			$cek_dsn = $this->md->dsn_get_where(array("id_dsn"=>$dsn));
			if($dsn){
				//panggil array kriteria untuk mengetahui input
				$kriteria = $this->kri->kri_get();

				$penilaian = $this->mm->get_data_all_where("penilaian", array("nidn"=>$dsn));
				$count_penilaian = count($penilaian);

				if(!($count_penilaian > 0)){
					//cek kriteria, kososng tidak jika tidak maka foreach untuk mengambil post
					if(!empty($kriteria)){

					//set var untuk status kriteria jika terjadi gagal input point
						$sts_insert_kri = false;
						foreach ($kriteria as $val_kriteria) {
					// $array_sub_kri[$val_kriteria->id_kri] 
								
					//set input kemudian cek untuk mengetahui semua nilai masuk atau tidak, jika masuk maka status akan true,
					//jika tidak maka status akan false dan keluar paksa dari perulangan
							$data_point = array("id_nilai"=>"", "nidn"=>$dsn, "id_kri"=>$val_kriteria->id_kri, "id_sub_kri"=>$_POST["kri_".$val_kriteria->id_kri]);
							if($this->md->insert_penilaian($data_point)){
								$sts_insert_kri = true;
							}else{
								$sts_insert_kri = false;
								break;
							}
						}

					//set status delete untuk pastika semua data yang gagal input terdelete
					//status gagal delete false 
						$sts_delete_all = false;
						if($sts_insert_kri == false){
							echo "insert tidak semua berhasil, hapus all penilaian dan dosen";
							if($this->md->dsn_delete(array("nidn"=>$nidn))){
								if($this->md->dell_all_penilaian(array("nidn"=>$nidn))){
									$sts_delete_all = true;
								}
							}

							if ($sts_delete_all == true) {
								echo "data dosen sudah di hapus, kembali kosong, delete berhasil";
							}else{
								echo "data dosen gagal dihapus, silahkan hapus manual";
							}

							$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
						}else {
							$msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
						}		
					
					}else{
						echo "insert dosen gagal";
					}
				}
					
			}
					
		}else {
			$msg_detail["dsn"] 		= form_error("dsn");
		}
		
		$msg_array = $this->response_message->default_mgs($msg_main, $msg_detail);
		// print_r("<pre>");
		// print_r($msg_array);
		$this->session->set_flashdata("response_send", $msg_array);
		redirect(base_url()."page/penilaian_dosen");
		
		// redirect(base_url()."maindsn");
	}



	public function get_penilaian_update(){
		// $id = "D10000001";
        $id = $this->input->post("nidn");
        $data = $this->mm->get_data_all_where("penilaian",array("nidn"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

    public function update_penilaian_dsn(){
    	// print_r("<pre>");
    	// print_r($_POST);
		$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
		$msg_detail = array("dsn" => "");

		if($this->val_penilaian_dsn()){
			$dsn = $this->input->post("dsn");
			$cek_dsn = $this->md->dsn_get_where(array("id_dsn"=>$dsn));
			if($dsn){
				//panggil array kriteria untuk mengetahui input
				$kriteria = $this->kri->kri_get();
				$count_kriteria = count($kriteria);

				$penilaian = $this->mm->get_data_all_where("penilaian", array("nidn"=>$dsn));
				$count_penilaian = count($penilaian);

				if($count_penilaian == $count_kriteria){
					//cek kriteria, kososng tidak jika tidak maka foreach untuk mengambil post
					if(!empty($kriteria)){

					//set var untuk status kriteria jika terjadi gagal input point
						$sts_insert_kri = false;
						foreach ($kriteria as $val_kriteria) {
					// $array_sub_kri[$val_kriteria->id_kri] 
								
					//set input kemudian cek untuk mengetahui semua nilai masuk atau tidak, jika masuk maka status akan true,
					//jika tidak maka status akan false dan keluar paksa dari perulangan
							$data_where = array("nidn"=>$dsn, "id_kri"=>$val_kriteria->id_kri);
							$data_point = array("id_sub_kri"=>$_POST["kri_".$val_kriteria->id_kri]);

							if($this->mm->update_data("penilaian", $data_point, $data_where)){
								$sts_insert_kri = true;
							}else{
								$sts_insert_kri = false;
								break;
							}
						}

					//set status delete untuk pastika semua data yang gagal input terdelete
					//status gagal delete false 
						$sts_delete_all = false;
						if($sts_insert_kri == false){
							// echo "insert tidak semua berhasil, hapus all penilaian dan dosen";
							if($this->md->dsn_delete(array("nidn"=>$nidn))){
								if($this->md->dell_all_penilaian(array("nidn"=>$nidn))){
									$sts_delete_all = true;
								}
							}

							if ($sts_delete_all == true) {
								// echo "data dosen sudah di hapus, kembali kosong, delete berhasil";
							}else{
								// echo "data dosen gagal dihapus, silahkan hapus manual";
							}

							$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
						}else {
							$msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
						}		
					}else{
						// echo "insert dosen gagal";
					}
				}
			}
		}else {
			$msg_detail["dsn"] 		= form_error("dsn");
		}
		
		$msg_array = $this->response_message->default_mgs($msg_main, $msg_detail);
		print_r(json_encode($msg_array));
		// print_r($msg_array);
		// $this->session->set_flashdata("response_send", $msg_array);
	}

	public function delete_penilaian_dsn(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $id_dsn =$this->input->post("dsn");

        $data_where = array("nidn"=> $id_dsn);
        if($this->mm->delete_data("penilaian",$data_where)){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }

#=================================================================================================#
#-------------------------------------------main_penilaian_dosen----------------------------------#
#=================================================================================================#
}
