<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainhome extends CI_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->library("response_message");
        $this->load->model("main/mainmodel", "mm");

        if(isset($_SESSION["admin_lv_1"])){
            if($this->session->userdata("admin_lv_1")["is_log"] != 1){
                redirect(base_url()."admin/login");
            }
        }else{
            redirect(base_url()."admin/login");
        }
	}
    
    public function index(){
        $data["page"] = "home";
        $data["count_dsn"] = count($this->mm->get_data_all("dsn"));
        $data["count_kri"] = count($this->mm->get_data_all("kriteria"));
        $data["count_kri_sub"] = count($this->mm->get_data_all("kriteria_sub"));

        $data["hasil"] = $this->index_show_result();
        $this->load->view('index', $data);
    }

    public function index_show_result(){
        $master_kriteria = $this->mm->get_data_all("kriteria");
        $master_dosen = $this->mm->get_data_all("dsn");

        $distict_nidn_penilaian = $this->mm->get_data_all_distict("penilaian", "nidn");

        $array_dsn = array();
        foreach ($master_dosen as $key => $value) {
            $array_dsn[$value->id_dsn] = $value;
        }

        #------------------------------------penilaian-------------------------

            $width_t_penilaian = 100 / count($master_kriteria)+1;

            $array_kriteria = array();
            
            foreach ($master_kriteria as $r_master_kriteria => $v_master_kriteria) {
                $array_kriteria[$v_master_kriteria->id_kri] = $v_master_kriteria; 
            
            }
            

            $array_penilaian_row = array();
            $array_penilaian_col = array();

            
            foreach ($distict_nidn_penilaian as $r_master_dosen => $v_master_dosen) {
                $array_dsn[$v_master_dosen->nidn] = $v_master_dosen;
                $data_penilaian  = $this->mm->get_full_penilaian(array("nidn"=>$v_master_dosen->nidn));
                foreach ($data_penilaian as $r_data_penilaian => $v_data_penilaian) {
                    $array_penilaian_row[$v_master_dosen->nidn][$v_data_penilaian->id_kri] = $v_data_penilaian->val_sub;
                    $array_penilaian_col[$v_data_penilaian->id_kri][$v_master_dosen->nidn] = $v_data_penilaian->val_sub;
                }
            }
            

            // print_r("<br>------------------------master_dosen-----------------------------<br>");
            // print_r($array_dsn);
            // print_r("<br>------------------------master_kriteria--------------------------<br>");
            // print_r($array_kriteria);
            // print_r("<br>------------------------penilaian_row----------------------------<br>");
            // print_r($array_penilaian_row);
            // print_r("<br>------------------------penilaian_col----------------------------<br>");
            // print_r($array_penilaian_col);
        #------------------------------------penilaian-------------------------

        #------------------------------------normalisasi-----------------------
            $width_t_var_normalisasi = 100 / 2;

            $array_var_normalisasi = array();
            foreach ($array_penilaian_col as $r_array_penilaian_col => $v_array_penilaian_col) {
                $count_pow = 0;
                foreach ($v_array_penilaian_col as $key => $value) {
                    $count_pow += pow((double)$value,2);
                }
                $array_var_normalisasi[$r_array_penilaian_col] = sqrt($count_pow);
                
            }


                        
            $array_normalisasi = array();
            $array_normalisasi_x_bobot = array();

            foreach ($array_penilaian_col as $r_array_penilaian_col => $v_array_penilaian_col) {
                foreach ($v_array_penilaian_col as $key => $value) {
                    $array_normalisasi[$r_array_penilaian_col][$key] = $value/$array_var_normalisasi[$r_array_penilaian_col];
                    $array_normalisasi_x_bobot[$r_array_penilaian_col][$key] = ($value/$array_var_normalisasi[$r_array_penilaian_col]) * $array_kriteria[$r_array_penilaian_col]->bobot;
                    
                }
            }

            // print_r("<br>------------------------var_normalisasi---------------------------<br>");
            // print_r($array_var_normalisasi);

            // print_r("<br>------------------------normalisasi-------------------------------<br>");
            // print_r($array_normalisasi);

            // print_r("<br>------------------------array_normalisasi_x_bobot-----------------<br>");
            // print_r($array_normalisasi_x_bobot);

            // print_r("<br>------------------------data_normalisasi--------------------------<br>");
            // print_r($data_t_normalisasi);

            // print_r("<br>------------------------data_normalisasi_x_bobot------------------<br>");
            // print_r($data_t_normalisasi_x_bobot);

                        
        #------------------------------------normalisasi-----------------------


        #------------------------------------Matriks Solusi Ideal Positif & Negatif------

            $array_solusi_pos = array();
            $array_solusi_neg = array();

            foreach ($array_normalisasi_x_bobot as $key => $value) {
                if($array_kriteria[$key]->tipe_kri == 0){
                    $array_solusi_pos[$key] = min($value);
                    $array_solusi_neg[$key] = max($value);
                }else{
                    $array_solusi_pos[$key] = max($value);
                    $array_solusi_neg[$key] = min($value);
                }

            }

            

            // print_r("<br>------------------------solusi_positif-------------------------<br>");
            // print_r($array_solusi_pos);
            // print_r("<br>------------------------solusi_negatif-------------------------<br>");
            // print_r($array_solusi_neg);

        #------------------------------------Matriks Solusi Ideal Positif & Negatif------

        #------------------------------------Jarak Antara Alternatif Dengan Solusi -----------------------

            $array_jarak_pos_pow = array();
            $array_jarak_neg_pow = array();

            foreach ($array_normalisasi_x_bobot as $r_array_normalisasi_x_bobot => $v_array_normalisasi_x_bobot) {
                foreach ($v_array_normalisasi_x_bobot as $key => $value) {
                    // if(isset($array_jarak_pos[$key])){
                    //  print_r(pow(((double)$value-(double)$array_solusi_pos[$r_array_normalisasi_x_bobot]), 2).", ");
                        $array_jarak_pos_pow[$key][$r_array_normalisasi_x_bobot] = pow(((double)$value-(double)$array_solusi_pos[$r_array_normalisasi_x_bobot]), 2);
                        $array_jarak_neg_pow[$key][$r_array_normalisasi_x_bobot] = pow(((double)$value-(double)$array_solusi_neg[$r_array_normalisasi_x_bobot]), 2);
                    // }else{
                    //  $array_jarak_pos_pow[$key] = pow(((double)$value-(double)$array_solusi_pos[$r_array_normalisasi_x_bobot]), 2);
                    //  print_r(pow(((double)$value-(double)$array_solusi_pos[$r_array_normalisasi_x_bobot]), 2).", ");
                        // $array_jarak_neg_pow[$key] = pow(((double)$value-(double)$array_solusi_neg[$r_array_normalisasi_x_bobot]), 2);
                    // }
                }
                // print_r("<br>");
            }

            $array_jarak_pos = array();
            $array_jarak_neg = array();

            

            foreach ($array_jarak_neg_pow as $key => $value) {
                // print_r($array_jarak_pos_pow);
                $array_jarak_neg[$key] = sqrt(array_sum($value));
                

                $array_jarak_pos[$key] = sqrt(array_sum($array_jarak_pos_pow[$key]));
                

                // sqrt((double)$value);
                // $array_jarak_pos[$key] = sqrt((double)$array_jarak_pos_pow[$key]);
            }

            


            // print_r("<br>------------------------jarak_pos_pow-------------------------<br>");
            // print_r($array_jarak_pos_pow);
            // print_r("<br>------------------------jarak_neg_pow-------------------------<br>");
            // print_r($array_jarak_neg_pow);
            // print_r("<br>------------------------jarak_pos-------------------------<br>");
            // print_r($array_jarak_pos);
            // print_r("<br>------------------------jarak_neg-------------------------<br>");
            // print_r($array_jarak_neg);
        #------------------------------------Jarak Antara Alternatif Dengan Solusi -----------------------


        #------------------------------------Penentuan nilai refrensi-----------------------
            $array_hasil_analisa = array();

            $this->db->query("DELETE FROM dummy_hasil WHERE 1");
            foreach ($array_jarak_neg as $key => $value) {
                $array_hasil_analisa[$key] = (double)$value / ((double)$value+(double)$array_jarak_pos[$key]);
                $this->mm->insert_data("dummy_hasil", array("nidn"=>$key, "hasil"=> (double)$value / ((double)$value+(double)$array_jarak_pos[$key])));
            }


            // $data["page"] = "page_hasil";
            $data = $this->mm->get_hasil();
            // $this->load->view("index", $data);

            return $data;
            // print_r("<pre>");
            // print_r($data);
            
            // print_r("<br>------------------------hasil_analisa-------------------------<br>");
            // print_r($array_hasil_analisa);
        #------------------------------------Penentuan nilai refrensi-----------------------



    }
}
?>