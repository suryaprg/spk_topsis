<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainkriteria extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("kriteria", "kri");
		$this->load->model("Main_dsn", "md");
		$this->load->model("Main_perhitungan", "mp");
		$this->load->model("Main_ahp", "ma");

		$this->load->model("main/mainmodel", "mm");

        $this->load->library("response_message");
        
        if(isset($_SESSION["admin_lv_1"])){
			if($this->session->userdata("admin_lv_1")["is_log"] != 1){
				redirect(base_url()."admin/login");
			}
		}else{
			redirect(base_url()."admin/login");
		}
	}

#=================================================================================================#
#-------------------------------------------main_kriteria-----------------------------------------#
#=================================================================================================#
	public function index_kri(){
		$data["page"] 			= "page_kriteria";
		$data["list_kriteria"] 	= $this->kri->kri_get();

		$this->load->view("index",$data);
	}

	private function validate_kri(){
        $config_val_input = array(
                array(
                    'field'=>'kri',
                    'label'=>'Kriteria',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tipe_kri',
                    'label'=>'tipe_kri',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'bobot',
                    'label'=>'bobot',
                    'rules'=>'required|numeric|less_than[11]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'less_than[11]'=>"%s ".$this->response_message->get_error_msg("MAX_10")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function insert_kri(){
		$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
		$msg_detail = array("kri" => "", "tipe_kri" => "", "bobot" => "");

		if($this->validate_kri()){
			$kri = $this->input->post("kri");
			$tipe_kri = $this->input->post("tipe_kri");
			$bobot = $this->input->post("bobot");

			$insert = $this->db->query("select insert_key_kri('".$kri."','".$tipe_kri."', '".$bobot."') as id_kri;")->row_array();

			$status_insert_kri = false;

			#insert kriteria
			if($insert){
                $send = array(
                        "id_kri"=>$insert["id_kri"],
                        "id_sub_kri"=>"",
                        "ket_sub"=>"dummy",
                        "val_sub"=>"1",
                    );

                $insert_sub_kri = $this->kri->sub_kri_insert($send);
                // print_r($insert["id_kri"]);
                if($insert_sub_kri){
                    $get_id_sub = $this->mm->get_data_all_where("kriteria_sub", array("id_kri"=>$insert["id_kri"]))[0]->id_sub_kri;
                    // print_r($get_id_sub);

                    $dsn_distict = $this->mm->get_data_all_distict("penilaian", "nidn");
                    foreach ($dsn_distict as $r_dsn_distict => $v_dsn_distict) {
                        $send_penilaian = array("id_nilai"=>"", 
                                            "nidn"=>$v_dsn_distict->nidn,
                                            "id_kri"=>$insert["id_kri"],
                                            "id_sub_kri"=>$get_id_sub);
                        $insert_penilaian = $this->mm->insert_data("penilaian",$send_penilaian);
                    
                    }
                    
                    $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
			}

		}else {
			$msg_detail["kri"] 		= form_error("kri");
			$msg_detail["tipe_kri"] = form_error("tipe_kri");
			$msg_detail["bobot"] 	= form_error("bobot");
		}

		$msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
		print_r(json_encode($msg_array));
        // redirect(base_url()."main/kriteria");
	}

    public function get_kri_update(){
        $id = $this->input->post("id_kri");
        $data = $this->mm->get_data_each("kriteria",array("id_kri"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

	private function validate_update_kri(){
        $config_val_input = array(
                array(
                    'field'=>'kri',
                    'label'=>'Kriteria',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tipe_kri',
                    'label'=>'tipe_kri',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'bobot',
                    'label'=>'bobot',
                    'rules'=>'required|numeric|less_than[11]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'less_than[11]'=>"%s ".$this->response_message->get_error_msg("MAX_10")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_kri(){
    	$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
		$msg_detail = array("kri" => "", "tipe_kri" => "", "bobot" => "");

		if($this->validate_kri()){
			$id_kri = $this->input->post("id_kri");

			$kri = $this->input->post("kri");
			$tipe_kri = $this->input->post("tipe_kri");
			$bobot = $this->input->post("bobot");

			$data_set = array("ket_kri"=> $kri,
    							"tipe_kri"=> $tipe_kri,
    							"bobot"=> $bobot
    						);

    		$data_where = array("id_kri"=>$id_kri);

			$update = $this->kri->kri_update($data_set, $data_where);

			#insert kriteria
			if($update){
				$msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
			}

		}else {
			$msg_detail["kri"] 		= form_error("kri");
			$msg_detail["tipe_kri"] = form_error("tipe_kri");
			$msg_detail["bobot"] 	= form_error("bobot");
		}

		$msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
		print_r(json_encode($msg_array));
    }
	
	public function delete_kri(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $id_kri =$this->input->post("id_kri");

        $data_where = array("id_kri"=> $id_kri);
        if($this->mm->delete_data("penilaian",$data_where)){
            if($this->mm->delete_data("kriteria_sub",$data_where)){
                if($this->kri->kri_delete($data_where)){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=================================================================================================#
#-------------------------------------------main_kriteria-----------------------------------------#
#=================================================================================================#
	
#=================================================================================================#
#-------------------------------------------main_sub_kriteria-------------------------------------#
#=================================================================================================#
	public function index_kri_sub(){
		$data["page"] = "page_sub_kriteria";
		$data["kriteria"] = $this->kri->kri_get();
		$data["sub_kriteria"] = $this->kri->sub_kri_get();
		$this->load->view("index",$data);
	}

	private function validate_sub_kri(){
        $config_val_input = array(
                array(
                    'field'=>'kri',
                    'label'=>'Kriteria',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'sub_kri',
                    'label'=>'Sub. Kriteria',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'bobot',
                    'label'=>'bobot',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function insert_sub_kri(){
		$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
		$msg_detail = array(
					"kri" => "",
					"sub_kri" => "",
					"bobot" => ""
				);
		if($this->validate_sub_kri()){
			$kri = $this->input->post("kri");
			$sub_kri = $this->input->post("sub_kri");
			$point = $this->input->post("bobot");

			$send = array(
					"id_kri"=>$kri,
					"id_sub_kri"=>"",
					"ket_sub"=>$sub_kri,
					"val_sub"=>$point,
				);

			$insert = $this->kri->sub_kri_insert($send);
			if($insert){
				$msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
			}
		}else {
			$msg_detail["kri"] = form_error("kri");
			$msg_detail["sub_kri"] = form_error("sub_kri");
			$msg_detail["bobot"] = form_error("bobot");

		}
		$msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
		// redirect(base_url()."main/subkriteria");
		print_r(json_encode($msg_array));
	}

	public function get_sub_kri_update(){
        $id = $this->input->post("id_sub_kri");
        $data = $this->mm->get_data_each("kriteria_sub",array("id_sub_kri"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

	private function validate_update_sub_kri(){
        $config_val_input = array(
                array(
                    'field'=>'kri',
                    'label'=>'Kriteria',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'sub_kri',
                    'label'=>'Sub. Kriteria',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'bobot',
                    'label'=>'bobot',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_sub_kri(){
    	$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
		$msg_detail = array("kri" => "", "sub_kri" => "", "bobot" => "");

		if($this->validate_sub_kri()){
			// print_r($_POST);
			$id_sub_kri = $this->input->post("id_sub_kri");

			$kri = $this->input->post("kri");
			$sub_kri = $this->input->post("sub_kri");
			$bobot = $this->input->post("bobot");

			$data_set = array("id_kri"=> $kri,
    							"ket_sub"=> $sub_kri,
    							"val_sub"=> $bobot
    						);

    		$data_where = array("id_sub_kri"=>$id_sub_kri);

			$update = $this->kri->sub_kri_update($data_set, $data_where);

			#insert kriteria
			if($update){
				$msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
			}

		}else {
			$msg_detail["kri"] 		= form_error("kri");
			$msg_detail["sub_kri"] = form_error("sub_kri");
			$msg_detail["bobot"] 	= form_error("bobot");
		}

		$msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
		print_r(json_encode($msg_array));
	}

	public function delete_sub_kri(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $id_sub_kri =$this->input->post("id_sub_kri");

        $data_where = array("id_sub_kri"=> $id_sub_kri);
        $cek_sub_kri  = $this->mm->get_data_each("kriteria_sub", $data_where);

        $get_sub_from_kri = $this->mm->get_data_all_where("kriteria_sub", array("id_kri"=>$cek_sub_kri["id_kri"]));
        

        if(count($get_sub_from_kri)<2){
            if($this->mm->delete_data("penilaian", array("id_kri"=>$cek_sub_kri["id_kri"]))){
                if($this->mm->delete_data("kriteria", array("id_kri"=>$cek_sub_kri["id_kri"]))){
                    if($this->mm->delete_data("kriteria_sub", array("id_kri"=>$cek_sub_kri["id_kri"]))){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                    }
                }
            }
        }else {
            $get_id_sub_kri = $this->mm->get_data_each("kriteria_sub", array("id_kri"=>$cek_sub_kri["id_kri"], 
                                                                        "id_sub_kri!="=> $id_sub_kri))["id_sub_kri"];

            if($this->mm->update_data("penilaian", 
                                        array("id_sub_kri"=>$get_id_sub_kri), 
                                        array("id_kri"=>$cek_sub_kri["id_kri"], 
                                                "id_sub_kri"=> $id_sub_kri))){
                if($this->kri->sub_kri_delete($data_where)){ 
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }
            
        }
        
        
        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=================================================================================================#
#-------------------------------------------main_sub_kriteria-------------------------------------#
#=================================================================================================#
}
