<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmhs extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("Main_mhs", "mhs");
        $this->load->library("response_message");
        
        if(isset($_SESSION["admin_lv_1"])){
			if($this->session->userdata("admin_lv_1")["is_log"] != 1){
				redirect(base_url()."admin/login");
			}
		}else{
			redirect(base_url()."admin/login");
		}
	}

	public function index(){
		$data["page"] = "mhs";
		$data["mhs"] = $this->mhs->mhs_get();
		$this->load->view('main_index',$data);
	}

	public function index_update($nim){
		$data["mhs"] = $this->mhs->mhs_get_where(array("nim"=>$nim));
		//print_r($data);
		$this->load->view('mhs/mhs_index_update',$data);

	}

	private function validate_mhs(){
        $config_val_input = array(
                array(
                    'field'=>'nim',
                    'label'=>'NIM',
                    'rules'=>'required|is_unique[mhs.nim]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("NIM_AVAIL")
                    )
                       
                ),array(
                    'field'=>'nama',
                    'label'=>'Nama Mahasiswa',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'smt',
                    'label'=>'Semester',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'sks',
                    'label'=>'SKS',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'tlp',
                    'label'=>'Telepon|numeric',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    private function validate_mhs_up(){
        $config_val_input = array(
                array(
                    'field'=>'nama',
                    'label'=>'Nama Mahasiswa',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'smt',
                    'label'=>'Semester',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'sks',
                    'label'=>'SKS',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'tlp',
                    'label'=>'Telepon|numeric',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }



	public function insert_mhs(){
		if($this->validate_mhs()){
			$nim = $this->input->post("nim");
			$nama = $this->input->post("nama");
			$smt = $this->input->post("smt");
			$sks = $this->input->post("sks");
			$tlp = $this->input->post("tlp");

			$send = array(
						"nim"=>$nim,
						"nama"=>$nama,
						"smester"=>$smt,
						"sks"=>$sks,
						"tlp"=>$tlp
					);

			$insert = $this->mhs->mhs_insert($send);
			if($insert){
				$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
			}else {
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
			}
			$msg_array = $this->response_message->default_mgs($main_msg,null);
            $this->session->set_flashdata("response_mhs", $msg_array);

		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
			$detail_msg = array(
					"nim" => form_error("nim"),
					"nama" => form_error("nama"),
					"smester" => form_error("smester"),
					"sks" => form_error("sks"),
					"tlp" => form_error("tlp")
					);
			$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
            $this->session->set_flashdata("response_mhs", $msg_array);

		}
		redirect(base_url()."main/mhs");
	}

	public function update_mhs(){
		if($this->validate_mhs_up()){
			$nim = $this->input->post("nim");
			$nama = $this->input->post("nama");
			$smt = $this->input->post("smt");
			$sks = $this->input->post("sks");
			$tlp = $this->input->post("tlp");

			$send = array(
						"nama"=>$nama,
						"smester"=>$smt,
						"sks"=>$sks,
						"tlp"=>$tlp
					);

			$update = $this->mhs->mhs_update($send, array("nim"=>$nim));
			if($update){
				$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
			}else {
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
			}
			$msg_array = $this->response_message->default_mgs($main_msg,null);
            $this->session->set_flashdata("response_mhs", $msg_array);

		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
			$detail_msg = array(
					"nim" => form_error("nim"),
					"nama" => form_error("nama"),
					"smester" => form_error("smester"),
					"sks" => form_error("sks"),
					"tlp" => form_error("tlp")
					);
			$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
            $this->session->set_flashdata("response_mhs", $msg_array);

		}

		// print_r("<pre>");
		// print_r($_SESSION);
		redirect(base_url()."main/mhs");
	}

	public function delete_mhs($nim){
		$delele = $this->mhs->mhs_delete(array("nim"=>$nim));
		if($delele){
			$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
		}
		$msg_array = $this->response_message->default_mgs($main_msg,null);
        $this->session->set_flashdata("response_mhs", $msg_array);
        redirect(base_url()."main/mhs");
	}


}
