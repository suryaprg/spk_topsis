<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindsn extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("kriteria", "kri");
		$this->load->model("main_dsn", "md");

        $this->load->library("response_message");

        if(isset($_SESSION["admin_lv_1"])){
			if($this->session->userdata("admin_lv_1")["is_log"] != 1){
				redirect(base_url()."admin/login");
			}
		}else{
			redirect(base_url()."admin/login");
		}
        
	}

#--------------------------------insert param-----------------------------------
	public function index(){
		$data["page"] = "dsn";
		$data["kriteria"] = $this->kri->kri_get();
		foreach ($data["kriteria"] as $val_kriteria) {
			$data["sub_kri"][$val_kriteria->id_kri] = $this->kri->sub_kri_get_where(array("id_kri"=>$val_kriteria->id_kri))->result();
		}

		$dsn = $this->md->dsn_get();

		$data["penilainan_all"] = array();
		foreach ($dsn as $r_dsn => $val_dsn) {
			$data["penilainan_all"][$r_dsn]["dsn"] = $val_dsn;
			$data["penilainan_all"][$r_dsn]["val"] = $this->md->get_all_penilaian(array("nidn"=>$val_dsn->nidn))->result();
		}
		
		$this->load->view('main_index',$data);
	}

#--------------------------------insert param-----------------------------------

	private function val_form_dsn(){
        $config_val_input = array(
                array(
                    'field'=>'nidn',
                    'label'=>'Nomor Induk Dosen',
                    'rules'=>'required|is_unique[dsn.nidn]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("NIDN_AVAIL")
                    )
                       
                ),array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'alamat',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tlp',
                    'label'=>'Telepon',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function inser_dsn(){
    	$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
        $msg_detail = array("nidn" => "",
                            "nama" => "",
                            "alamat" => "",
                            "tlp" => "");

        if ($this->val_form_dsn()) {
        	$nidn = $this->post->input("nidn");
        	$nama = mysql_real_escape_string($this->post->input("nama"));
        	$alamat = mysql_real_escape_string($this->post->input("alamat"));
        	$tlp = $this->post->input("tlp");

        	
        }else{
        	$msg_detail["nidn"] = strip_tags(form_error("nidn"));
        	$msg_detail["nama"] = strip_tags(form_error("nama"));
        	$msg_detail["alamat"] = strip_tags(form_error("alamat"));
        	$msg_detail["tlp"] = strip_tags(form_error("tlp"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $this->session->set_flashdata("response_login", $msg_array);
    }

	public function insert_param(){
		if($this->val_form_dsn()){
			$nama = $this->input->post("nama");
			$nidn = $this->input->post("nidn");

			//insert data dosen
			$data_dsn = array("nidn"=>$nidn, "nama"=>$nama);
			
			//jika berhasil maka insert data penilaian.
			if($this->md->dsn_insert($data_dsn)){

				//panggil array kriteria untuk mengetahui input
				$kriteria = $this->kri->kri_get();
				// $array_sub_kri = array();
				
				//cek kriteria, kososng tidak jika tidak maka foreach untuk mengambil post
				if(!empty($kriteria)){

					//set var untuk status kriteria jika terjadi gagal input point
					$sts_insert_kri = false;
					foreach ($kriteria as $val_kriteria) {
						// $array_sub_kri[$val_kriteria->id_kri] 
						
						//set input kemudian cek untuk mengetahui semua nilai masuk atau tidak, jika masuk maka status akan true,
						//jika tidak maka status akan false dan keluar paksa dari perulangan
						$data_point = array("id_nilai"=>"", "nidn"=>$nidn, "id_kri"=>$val_kriteria->id_kri, "id_sub_kri"=>$_POST[$val_kriteria->id_kri]);
						if($this->md->insert_penilaian($data_point)){
							$sts_insert_kri = true;
						}else{
							$sts_insert_kri = false;
							break;
						}
					}

					//set status delete untuk pastika semua data yang gagal input terdelete
					//status gagal delete false 
					$sts_delete_all = false;
					if($sts_insert_kri == false){
						echo "insert tidak semua berhasil, hapus all penilaian dan dosen";
						if($this->md->dsn_delete(array("nidn"=>$nidn))){
							if($this->md->dell_all_penilaian(array("nidn"=>$nidn))){
								$sts_delete_all = true;
							}
						}
						if ($sts_delete_all == true) {
							echo "data dosen sudah di hapus, kembali kosong, delete berhasil";
						}else{
							echo "data dosen gagal dihapus, silahkan hapus manual";
						}

						$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
					}else {
						$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
					}

					
				}
			}else{
				echo "insert dosen gagal";
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
			}			
		}else {
			echo "insert dosen gagal, fail input";
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
		}
		
		//print_r("<pre>");
		//print_r($array_sub_kri);
		$msg_array = $this->response_message->default_mgs($main_msg, null);
        $this->session->set_flashdata("response_dsn", $msg_array);

		redirect(base_url()."maindsn");
	}

#--------------------------------update param-----------------------------------

	public function index_update_dsn($nidn){
		$data["kriteria"] = $this->kri->kri_get();
		foreach ($data["kriteria"] as $val_kriteria) {
			$data["sub_kri"][$val_kriteria->id_kri] = $this->kri->sub_kri_get_where(array("id_kri"=>$val_kriteria->id_kri))->result();
		}

		$data["up_dsn"] = $this->md->dsn_get_where(array("nidn"=>$nidn));
		$data["up_point"] = $this->md->get_all_penilaian(array("nidn"=>$nidn))->result();

		$this->load->view('dosen/form_update_dsn',$data);
	}

	private function val_up_dsn(){
        $config_val_input = array(
                array(
                    'field'=>'nidn',
                    'label'=>'Nomor Induk Dosen',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function update_param(){
		if($this->val_up_dsn()){
			$nama = $this->input->post("nama");
			$nidn = $this->input->post("nidn");

			//insert data dosen
			$data_dsn = array("nama"=>$nama);
			
			//jika berhasil maka insert data penilaian.
			if($this->md->dsn_update($data_dsn, array("nidn"=>$nidn))){
				$dell_all_penilaian = $this->md->dell_all_penilaian(array("nidn"=>$nidn));
				if($dell_all_penilaian){
					//panggil array kriteria untuk mengetahui input
					$kriteria = $this->kri->kri_get();
					// $array_sub_kri = array();
					
					//cek kriteria, kososng tidak jika tidak maka foreach untuk mengambil post
					if(!empty($kriteria)){

						//set var untuk status kriteria jika terjadi gagal input point
						$sts_insert_kri = false;
						foreach ($kriteria as $val_kriteria) {
							// $array_sub_kri[$val_kriteria->id_kri] 
							
							//set input kemudian cek untuk mengetahui semua nilai masuk atau tidak, jika masuk maka status akan true,
							//jika tidak maka status akan false dan keluar paksa dari perulangan
							$data_point = array("id_nilai"=>"", "nidn"=>$nidn, "id_kri"=>$val_kriteria->id_kri, "id_sub_kri"=>$_POST[$val_kriteria->id_kri]);
							if($this->md->insert_penilaian($data_point)){
								$sts_insert_kri = true;
							}else{
								$sts_insert_kri = false;
								break;
							}
						}

						//set status delete untuk pastika semua data yang gagal input terdelete
						//status gagal delete false 
						$sts_delete_all = false;
						if($sts_insert_kri == false){
							echo "insert tidak semua berhasil, hapus all penilaian dan dosen";
							if($this->md->dsn_delete(array("nidn"=>$nidn))){
								if($this->md->dell_all_penilaian(array("nidn"=>$nidn))){
									$sts_delete_all = true;
								}
							}
							if ($sts_delete_all == true) {
								echo "data dosen sudah di hapus, kembali kosong, delete berhasil";
							}else{
								echo "data dosen gagal dihapus, silahkan hapus manual";
							}

							$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
						}else {
							$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
						}
					}
				}
			}else{
				echo "insert dosen gagal";
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
			}			
		}else {
			echo "insert dosen gagal, fail input";
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
		}
		
		$msg_array = $this->response_message->default_mgs($main_msg, null);
        $this->session->set_flashdata("response_dsn", $msg_array);
		//print_r("<pre>");
		//print_r($array_sub_kri);

		redirect(base_url()."maindsn");
	}

#--------------------------------delete dosen-----------------------------------


	public function delete_dsn($nidn){
		$sts_delete_all = false;
		if($this->md->dsn_delete(array("nidn"=>$nidn))){
			if($this->md->dell_all_penilaian(array("nidn"=>$nidn))){
				$sts_delete_all = true;
			}

			if ($sts_delete_all == true) {
				echo "data dosen sudah di hapus, kembali kosong, delete berhasil";
				$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
			}else{
				echo "data dosen gagal dihapus, silahkan hapus manual";
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
			}
		}

		$msg_array = $this->response_message->default_mgs($main_msg, null);
        $this->session->set_flashdata("response_dsn", $msg_array);

        redirect(base_url()."maindsn");
	}
}
