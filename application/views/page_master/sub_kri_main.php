            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Halaman Sub Kriteria</h3>
                </div>
                
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4 class="card-title">List Data Sub Kriteria</h4>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                            <i class="fa fa-plus-circle"></i> Tambah Sub Kriteria
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="7%">No</th>
                                                <th width="38%">Sub Kriteria</th>
                                                <th width="30%">Kriteria</th>
                                                <th width="10%">Bobot Sub Kriteria</th>
                                                <th width="15%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $no = 1;
                                                if($sub_kriteria){
                                                    foreach ($sub_kriteria as $r_sub_kriteria => $v_sub_kriteria){
                                                        
                                                        print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_sub_kriteria->ket_sub."</td>
                                                                <td>".$v_sub_kriteria->ket_kri."</td>
                                                                <td>".$v_sub_kriteria->val_sub."</td>
                                                                                                                                
                                                                <td>
                                                                <center>
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_data\" onclick=\"get_update_data('".$v_sub_kriteria->id_sub_kri."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$v_sub_kriteria->id_sub_kri."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Insert sub_Kriteria  -->
<!-- ============================================================== -->
<div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Tambah data Sub Kriteria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Sub Kriteria</label>
                        <input type="text" name="sub_kri" id="sub_kri" class="form-control form-control-line">
                        <a id="msg_sub_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Kriteria</label>
                        <select name="kri" id="kri" class="form-control form-control-line">
                            <?php
                                foreach ($kriteria as $r_kriteria => $v_kriteria) {
                                    $tipe_kri = "cost";
                                    if($v_kriteria->tipe_kri == 1){
                                        $tipe_kri = "benefit";
                                    }
                                    print_r("<option value=\"".$v_kriteria->id_kri."\">".$v_kriteria->ket_kri." - ".$tipe_kri."</option>");
                                }
                            ?>
                        </select>
                        <a id="msg_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Bobot Sub Kriteria</label>
                        <input type="number" name="bobot" id="bobot" max="10" class="form-control form-control-line">
                        <a id="msg_bobot" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_add_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Insert sub_Kriteria  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Update sub_Kriteria  -->
<!-- ============================================================== -->
<div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Ubah data Sub Kriteria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Sub Kriteria</label>
                        <input type="text" name="sub_kri" id="_sub_kri" class="form-control form-control-line">
                        <a id="_msg_sub_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Kriteria</label>
                        <select name="kri" id="_kri" class="form-control form-control-line">
                            <?php
                                foreach ($kriteria as $r_kriteria => $v_kriteria) {
                                    $tipe_kri = "cost";
                                    if($v_kriteria->tipe_kri == 1){
                                        $tipe_kri = "benefit";
                                    }
                                    print_r("<option value=\"".$v_kriteria->id_kri."\">".$v_kriteria->ket_kri." - ".$tipe_kri."</option>");
                                }
                            ?>
                        </select>
                        <a id="_msg_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Bobot Sub Kriteria</label>
                        <input type="number" name="bobot" id="_bobot" max="10" class="form-control form-control-line">
                        <a id="_msg_bobot" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_up_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Update sub_Kriteria  -->
<!-- ============================================================== -->


<script type="text/javascript">
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================
    $("#btn_add_data").click(function(){
            var data_main =  new FormData();
            data_main.append('kri'      , $("#kri").val());
            data_main.append('sub_kri'  , $("#sub_kri").val());
            data_main.append('bobot'    , $("#bobot").val());
                                                    
            $.ajax({
                url: "<?php echo base_url()."master/mainkriteria/insert_sub_kri";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                alert(main_msg.msg);
                window.location.href = "<?php print_r(base_url());?>page/sub_kriteria";
            }else{
                $("#msg_kri").html(detail_msg.kri);
                $("#msg_sub_kri").html(detail_msg.sub_kri);
                $("#msg_bobot").html(detail_msg.bobot);                
            }
        }
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================

var id_data_glob = "";

//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_kri").val("");
        $("#_sub_kri").val("");
        $("#_bobot").val("");

        id_admin_glob = "";
    }

    function get_update_data(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('id_sub_kri' , param);
                                        
        $.ajax({
            url: "<?php echo base_url()."master/mainkriteria/get_sub_kri_update";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                set_val_update(res);
                
            }
        });
    }

    function set_val_update(res){
        var res_pemohon = JSON.parse(res.toString());

        if(res_pemohon.status == true){
            var id_data_chahce = res_pemohon.val_response.id_sub_kri;
            $("#_sub_kri").val(res_pemohon.val_response.ket_sub);
            $("#_kri").val(res_pemohon.val_response.id_kri);
            $("#_bobot").val(res_pemohon.val_response.val_sub);
            
            id_data_glob = res_pemohon.val_response.id_sub_kri;

            $("#update_data").modal("show");
        }else {
            clear_from_update();
        }
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    $("#btn_up_data").click(function() {
        var confirm_msg = confirm("Apa anda yakin merubah data ini ?");
        if(confirm_msg) {
            var data_main = new FormData();
            data_main.append('id_sub_kri', id_data_glob);

            data_main.append('kri' , $("#_kri").val());
            data_main.append('sub_kri'     , $("#_sub_kri").val());
            data_main.append('bobot'   , $("#_bobot").val());

            $.ajax({
                    url: "<?php echo base_url()."master/mainkriteria/update_sub_kri";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        response_update(res);
                    }
            });
        }
        
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        console.log(data_json);
        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            window.location.href = "<?php print_r(base_url());?>page/sub_kriteria";
        } else {
            $("#_msg_kri").html(detail_msg.kri);
            $("#_msg_sub_kri").html(detail_msg.sub_kri);
            $("#_msg_bobot").html(detail_msg.bobot);
        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_data(param){
            var confirm_msg = confirm("Jika anda menghapus data ini maka seluruh data yang berhubungan dengan data ini akan hilang, Apa anda yakin menghapus data ini ?");
            if(confirm_msg) {
                var data_main =  new FormData();
                data_main.append('id_sub_kri', param);
                                                    
                $.ajax({
                    url: "<?php echo base_url()."master/mainkriteria/delete_sub_kri";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        console.log(res); 
                        alert("hapus data berhasil !, seluruh data yang berkaitan dengan data ini akan terhapus..");
                        location.href="<?php print_r(base_url());?>page/sub_kriteria";
                    }
                });   
            }
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================

</script>