            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Halaman Kriteria</h3>
                </div>
                
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4 class="card-title">List Data Kriteria</h4>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                            <i class="fa fa-plus-circle"></i> Tambah Kriteria
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kriteria</th>
                                                <th>Tipe Kriteria</th>
                                                <th>Bobot</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $no = 1;
                                                if($list_kriteria){
                                                    foreach ($list_kriteria as $r_list_kriteria => $v_list_kriteria){
                                                        $tipe_kri = "cost";
                                                        if($v_list_kriteria->tipe_kri == 1){
                                                            $tipe_kri = "benefit";
                                                        }
                                                        print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_list_kriteria->ket_kri."</td>
                                                                <td>".$tipe_kri."</td>
                                                                <td>".$v_list_kriteria->bobot."</td>
                                                                                                                                
                                                                <td>
                                                                <center>
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_data\" onclick=\"get_update_data('".$v_list_kriteria->id_kri."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$v_list_kriteria->id_kri."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Insert Kriteria  -->
<!-- ============================================================== -->
<div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Tambah data Kriteria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Kriteria</label>
                        <input type="text" name="kri" id="kri" class="form-control form-control-line">
                        <a id="msg_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Tipe Kriteria</label>
                        <select name="tipe_kri" id="tipe_kri" class="form-control form-control-line">
                            <option value="0">Cost</option>
                            <option value="1">Benefit</option>
                        </select>
                        <a id="msg_tipe_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Bobot</label>
                        <input type="number" name="bobot" id="bobot" max="10" class="form-control form-control-line">
                        <a id="msg_bobot" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_add_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Insert Kriteria  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Update Kriteria  -->
<!-- ============================================================== -->
<div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Ubah data Kriteria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Kriteria</label>
                        <input type="text" name="kri" id="_kri" class="form-control form-control-line">
                        <a id="_msg_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Tipe Kriteria</label>
                        <select name="tipe_kri" id="_tipe_kri" class="form-control form-control-line">
                            <option value="0">Cost</option>
                            <option value="1">Benefit</option>
                        </select>
                        <a id="_msg_tipe_kri" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Bobot</label>
                        <input type="number" name="bobot" id="_bobot" max="10" class="form-control form-control-line">
                        <a id="_msg_bobot" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_up_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Update Kriteria  -->
<!-- ============================================================== -->


<script type="text/javascript">
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================
    $("#btn_add_data").click(function(){
            var data_main =  new FormData();
            data_main.append('kri'      , $("#kri").val());
            data_main.append('tipe_kri' , $("#tipe_kri").val());
            data_main.append('bobot'    , $("#bobot").val());
                                                    
            $.ajax({
                url: "<?php echo base_url()."master/mainkriteria/insert_kri";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                alert(main_msg.msg);
                window.location.href = "<?php print_r(base_url());?>page/kriteria";
            }else{
                $("#msg_kri").html(detail_msg.kri);
                $("#msg_tipe_kri").html(detail_msg.tipe_kri);
                $("#msg_bobot").html(detail_msg.bobot);                
            }
        }
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================

var id_data_glob = "";

//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_kri").val("");
        $("#_tipe_kri").val("");
        $("#_bobot").val("");

        id_admin_glob = "";
    }

    function get_update_data(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('id_kri' , param);
                                        
        $.ajax({
            url: "<?php echo base_url()."master/mainkriteria/get_kri_update";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                set_val_update(res);
                
            }
        });
    }

    function set_val_update(res){
        var res_pemohon = JSON.parse(res.toString());

        if(res_pemohon.status == true){
            var id_data_chahce = res_pemohon.val_response.id_kri;
            $("#_kri").val(res_pemohon.val_response.ket_kri);
            $("#_tipe_kri").val(res_pemohon.val_response.tipe_kri);
            $("#_bobot").val(res_pemohon.val_response.bobot);
            
            id_data_glob = res_pemohon.val_response.id_kri;

            $("#update_data").modal("show");
        }else {
            clear_from_update();
        }
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    $("#btn_up_data").click(function() {
        var confirm_msg = confirm("Apa anda yakin merubah data ini ?");
        if(confirm_msg) {
            var data_main = new FormData();
            data_main.append('id_kri', id_data_glob);

            data_main.append('kri' , $("#_kri").val());
            data_main.append('tipe_kri'     , $("#_tipe_kri").val());
            data_main.append('bobot'   , $("#_bobot").val());

            $.ajax({
                    url: "<?php echo base_url()."master/mainkriteria/update_kri";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        response_update(res);
                    }
            });
        }
        
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        console.log(data_json);
        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            window.location.href = "<?php print_r(base_url());?>page/kriteria";
        } else {
            $("#_msg_kri").html(detail_msg.kri);
            $("#_msg_tipe_kri").html(detail_msg.tipe_kri);
            $("#_msg_bobot").html(detail_msg.bobot);
        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_data(param){
            var confirm_msg = confirm("Jika anda menghapus data ini maka seluruh data yang berhubungan dengan data ini akan hilang, Apa anda yakin menghapus data ini ?");
            if(confirm_msg) {
                var data_main =  new FormData();
                data_main.append('id_kri', param);
                                                    
                $.ajax({
                    url: "<?php echo base_url()."master/mainkriteria/delete_kri";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        console.log(res); 
                        alert("hapus data berhasil !, seluruh data yang berkaitan dengan data ini akan terhapus..");
                        location.href="<?php print_r(base_url());?>page/kriteria";
                    }
                });   
            }
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================

</script>