<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Halaman Dosen</h3>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i>
        </button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="card-title">List Data Dosen</h4>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data"> <i class="fa fa-plus-circle"></i> Tambah Dosen</button>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor Dosen</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Telephon</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                        if(isset($list_data)){
                                            if(!empty($list_data)){
                                                $no = 1;
                                                foreach ($list_data as $r_list_data => $v_list_data) {
                                                    print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_list_data->nidn."</td>
                                                                <td>".$v_list_data->nama."</td>
                                                                <td>".$v_list_data->alamat."</td>
                                                                <td>".$v_list_data->tlp."</td>
                                                                
                                                                <td>
                                                                <center>
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_data\" onclick=\"get_update_data('".$v_list_data->id_dsn."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$v_list_data->id_dsn."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                }
                                            }
                                        }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Insert Dosen  -->
<!-- ============================================================== -->
<div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Tambah data Dosen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Dosen</label>
                        <input type="text" name="nama_dsn" id="nama_dsn" class="form-control form-control-line">
                        <a id="msg_nama_dsn" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Nomor Induk Dosen</label>
                        <input type="text" name="nidn" id="nidn" class="form-control form-control-line">
                        <a id="msg_nidn" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" name="alamat" id="alamat" class="form-control form-control-line">
                        <a id="msg_alamat" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" name="tlp" id="tlp" class="form-control form-control-line">
                        <a id="msg_tlp" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_add_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Insert Dosen  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Update Dosen  -->
<!-- ============================================================== -->
<div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Form Ubah data Dosen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label>Nama Dosen</label>
                        <input type="text" name="nama_dsn" id="_nama_dsn" class="form-control form-control-line">
                        <a id="_msg_nama_dsn" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Nomor Induk Dosen</label>
                        <input type="text" name="nidn" id="_nidn" class="form-control form-control-line">
                        <a id="_msg_nidn" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" name="alamat" id="_alamat" class="form-control form-control-line">
                        <a id="_msg_alamat" style="color: red;"></a>
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" name="tlp" id="_tlp" class="form-control form-control-line">
                        <a id="_msg_tlp" style="color: red;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn_up_data" class="btn btn-info waves-effect">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- Update Dosen  -->
<!-- ============================================================== -->

<script type="text/javascript">
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================
    $("#btn_add_data").click(function(){
            var data_main =  new FormData();
            data_main.append('nama_dsn' , $("#nama_dsn").val());
            data_main.append('nidn'     , $("#nidn").val());
            data_main.append('alamat'   , $("#alamat").val());
            data_main.append('tlp'      , $("#tlp").val());
                                        
            $.ajax({
                url: "<?php echo base_url()."master/maindsn/insert_dosen";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                alert(main_msg.msg);
                window.location.href = "<?php print_r(base_url());?>page/dosen";
            }else{
                $("#msg_nama_dsn").html(detail_msg.nama_dsn);
                $("#msg_nidn").html(detail_msg.nidn);
                $("#msg_alamat").html(detail_msg.alamat);
                $("#msg_tlp").html(detail_msg.tlp);
                
            }
        }
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================

var id_data_glob = "";

//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_nama_dsn").val("");
        $("#_nidn").val("");
        $("#_alamat").val("");
        $("#_tlp").val("");

        id_admin_glob = "";
    }

    function get_update_data(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('id_dsn' , param);
                                        
        $.ajax({
            url: "<?php echo base_url()."master/maindsn/get_dsn_update";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                set_val_update(res);
                
            }
        });
    }

    function set_val_update(res, param){
        var res_pemohon = JSON.parse(res.toString());

        console.log(res_pemohon);

        if(res_pemohon.status == true){
            var id_data_chahce = res_pemohon.val_response.id_data;
            $("#_nama_dsn").val(res_pemohon.val_response.nama);
            $("#_nidn").val(res_pemohon.val_response.nidn);
            $("#_alamat").val(res_pemohon.val_response.alamat);
            $("#_tlp").val(res_pemohon.val_response.tlp);

            id_data_glob = res_pemohon.val_response.id_dsn;

            $("#update_data").modal("show");
        }else {
            clear_from_update();
        }
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    $("#btn_up_data").click(function() {
        var confirm_msg = confirm("Apa anda yakin merubah data ini ?");
        if(confirm_msg) {
            var data_main = new FormData();
            data_main.append('id_dsn', id_data_glob);

            data_main.append('nama_dsn' , $("#_nama_dsn").val());
            data_main.append('nidn'     , $("#_nidn").val());
            data_main.append('alamat'   , $("#_alamat").val());
            data_main.append('tlp'      , $("#_tlp").val());
                                            
            $.ajax({
                    url: "<?php echo base_url()."master/maindsn/update_dosen";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        response_update(res);
                    }
            });
        }
        
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            window.location.href = "<?php print_r(base_url());?>page/dosen";
        } else {
            $("#_msg_nama_dsn").html(detail_msg.nama);
            $("#_msg_nidn").html(detail_msg.nidn);
            $("#_msg_alamat").html(detail_msg.alamat);
            $("#_msg_tlp").html(detail_msg.tlp);
        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_data(param){
            var confirm_msg = confirm("Jika anda menghapus data ini maka seluruh data yang berhubungan dengan data ini akan hilang, Apa anda yakin menghapus data ini ?");
            if(confirm_msg) {
                var data_main =  new FormData();
                data_main.append('id_dsn', param);
                                                    
                $.ajax({
                    url: "<?php echo base_url()."master/maindsn/delete_dsn";?>",
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        console.log(res); 
                        alert("hapus data berhasil !, seluruh data yang berkaitan dengan data ini akan terhapus..");
                        location.href="<?php print_r(base_url());?>page/dosen";
                    }
                });   
            }
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================

</script>