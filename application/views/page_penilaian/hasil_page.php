            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Halaman Hasil Penilaian</h3>
                </div>
                
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4 class="card-title">List Data Hasil Penilaian</h4>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <button type="button" class="btn btn-info btn-rounded" id="btn_go_penilaian">
                                                <i class="fa fa-plus-circle"></i> Halaman Penilaian Dosen
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id Dosen</th>
                                                <th>Nama Dosen</th>
                                                <th>NIDN</th>
                                                <th>Hasil Penilaian</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                                if($hasil){
                                                    foreach ($hasil as $key => $value) {
                                                        print_r("<tr>
                                                                    <td>".$value->id_dsn."</td>
                                                                    <td>".$value->nama."</td>
                                                                    <td>".$value->nidn."</td>
                                                                    <td align=\"right\">".$value->hasil."</td>
                                                                </tr>");
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script type="text/javascript">
    $("#btn_go_penilaian").click(function(){
        window.location.href = "<?= base_url();?>page/penilaian_dosen";
    });
</script>