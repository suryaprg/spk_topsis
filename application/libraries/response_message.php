<?php
class response_message{
    
    public function get_error_msg($id_message){
        $errors_msg = array(
            "REQUIRED"=>"Tidak boleh kosong",
            "NUMBER"=>"Input Salah, Input harus berupa angka",
            "NUMBER_CHAR"=>"Input Salah, Input tidak diperkenankan selain berupa angka dan huruf",
            "EXACT_LENGHT_NIK"=>"Input harus 16 digit",
            "EMAIL"=>"Input Email tidak valid, mohon input dengan benar",
            "PASSWORD_LENGHT"=>"Input tidak sesuai, maksimal karakter adalah 15 dan minimal karakter adalah 5",
            "NIK_AVAIL"=>"Nik sudah terdaftar, satu Nik tidak bisa di gunakan untuk 2 akun berbeda",
            "NIM_AVAIL"=>"NIM sudah terdaftar, satu NIM tidak bisa di gunakan untuk 2 akun berbeda",
            "NIDN_AVAIL"=>"NIDN sudah terdaftar, satu NIDN tidak bisa di gunakan untuk 2 akun berbeda",
            
            "EMAIL_AVAIL"=>"Email sudah terdaftar, satu Email tidak bisa di gunakan untuk 2 akun berbeda",
            "INPUT_FAIL"=>"Input tidak tepat, mohon periksa input saudara kembali",
            "RE_PASSWORD_FAIL"=>"Mohon Ulangi password anda dengan benar",
            
            "UPDATE_FAIL"=>"Mohon maaf, Proses update gagal silahkan perikasa input saudara lagi",
            "INSERT_FAIL"=>"Mohon maaf, Proses insert gagal silahkan perikasa input saudara lagi",
            "DELETE_FAIL"=>"Mohon maaf, Proses delete gagal silahkan perikasa input saudara lagi",
            
            "LOG_FAIL"=>"Mohon maaf, Proses update gagal silahkan perikasa input saudara lagi",
            "MAX_10"=>"Mohon maaf, Nilai maksimal adalah 10"
        );
        
        return $errors_msg[$id_message];
    }
    
    public function get_success_msg($id_message){
        $succes_msg = array(
            "REG_SUC"=>"Permintaan registrasi saudara telah di terima, Silahkan buka email saudara dan klik tautan yang sudah kami kirim ke email saudara",
            "LOG_SUC"=>"Login Berhasil, Selamat datang di halaman admin",
            
            "UPDATE_PROF_SUC"=>"Permintaan perubahan saudara telah di terima",
            "UPDATE_PROF_SUC_EMAIL"=>"Permintaan perubahan saudara telah di terima, Silahkan buka email saudara dan klik tautan yang sudah kami kirim ke email saudara",
            
            "UPDATE_SUC"=>"Update berhasil",
            "INSERT_SUC"=>"Insert berhasil",
            "DELETE_SUC"=>"Delete berhasil"
        );
        
        return $succes_msg[$id_message];
    }
    
    public function default_mgs($msg_main, $msg_detail){
        return array(
                    "msg_main"=>$msg_main,
                    "msg_detail"=>$msg_detail
                );
    }
}
?>