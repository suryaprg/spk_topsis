-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Apr 2019 pada 05.38
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spk_topsis`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_dsn`(`nama` TEXT, `nidn` VARCHAR(32), `alamat` TEXT, `tlp` VARCHAR(13)) RETURNS varchar(12) CHARSET latin1
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from dsn;
        
  select id_dsn into last_key_item from dsn order by id_dsn desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("D","10000001");
  else
    set fix_key_item = concat("D",RIGHT(last_key_item,8)+1);
      
  END IF;
  
  insert into dsn values(fix_key_item, nidn, nama, alamat, tlp);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_key_kri`(`ket_kri` VARCHAR(50), `tipe_kri` ENUM('0','1'), `bobot` INT(2)) RETURNS varchar(20) CHARSET latin1
BEGIN
  declare last_key_kri varchar(20);
  select id_kri into last_key_kri from kriteria order by id_kri desc limit 1;
  
  insert into kriteria values(last_key_kri+1, ket_kri, tipe_kri, bobot);
 
  
  return last_key_kri+1;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_key_sub_kri`(`id_kri` INT(20), `ket_sub` VARCHAR(64), `val_sub` INT(20)) RETURNS varchar(20) CHARSET latin1
BEGIN
  declare last_key_sub_kri varchar(20);
  select id_sub_kri into last_key_sub_kri from kriteria_sub order by id_sub_kri desc limit 1;
  
  insert into kriteria_sub values(last_key_sub_kri+1, id_kri, ket_sub, val_sub);
 
  
  return last_key_sub_kri+1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `password`, `nama`, `nip`, `jabatan`) VALUES
('AD20180912', 'admin', 'aff8fbcbf1363cd7edc85a1e11391173', 'Surya Hanggara', '081230695774', 'Pengawas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dsn`
--

CREATE TABLE IF NOT EXISTS `dsn` (
  `id_dsn` varchar(10) NOT NULL,
  `nidn` varchar(10) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `tlp` varchar(13) NOT NULL,
  PRIMARY KEY (`id_dsn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dsn`
--

INSERT INTO `dsn` (`id_dsn`, `nidn`, `nama`, `alamat`, `tlp`) VALUES
('D10000001', '1231009871', 'sudirmans', 'malangs', '0812306957451'),
('D10000002', '1231009872', 'Dr. Radjiman Setyo', 'bantul', '081564879354'),
('D10000003', '1231009871', 'Darmono', 'donomulyo', '085654123987'),
('D10000004', '2132312321', 'dono', 'situbondo', '321656158987');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dummy_hasil`
--

CREATE TABLE IF NOT EXISTS `dummy_hasil` (
  `nidn` varchar(64) NOT NULL,
  `hasil` double NOT NULL,
  PRIMARY KEY (`nidn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dummy_hasil`
--

INSERT INTO `dummy_hasil` (`nidn`, `hasil`) VALUES
('D10000001', 0.74907385669506),
('D10000002', 0.4310701364113),
('D10000003', 0.29361962663355),
('D10000004', 0.74907385669506);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria`
--

CREATE TABLE IF NOT EXISTS `kriteria` (
  `id_kri` int(2) NOT NULL AUTO_INCREMENT,
  `ket_kri` varchar(64) NOT NULL,
  `tipe_kri` enum('0','1') NOT NULL,
  `bobot` int(2) NOT NULL,
  PRIMARY KEY (`id_kri`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `kriteria`
--

INSERT INTO `kriteria` (`id_kri`, `ket_kri`, `tipe_kri`, `bobot`) VALUES
(9, 'Pengalaman', '0', 9),
(10, 'Penguasaan Bahasa', '1', 5),
(11, 'Pendidikan', '0', 5),
(12, 'dsads', '0', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria_sub`
--

CREATE TABLE IF NOT EXISTS `kriteria_sub` (
  `id_sub_kri` int(11) NOT NULL AUTO_INCREMENT,
  `id_kri` int(11) NOT NULL,
  `ket_sub` varchar(64) NOT NULL,
  `val_sub` int(11) NOT NULL,
  PRIMARY KEY (`id_sub_kri`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data untuk tabel `kriteria_sub`
--

INSERT INTO `kriteria_sub` (`id_sub_kri`, `id_kri`, `ket_sub`, `val_sub`) VALUES
(51, 9, '1 - 3 th', 1),
(52, 10, '<= 2 bahasa', 1),
(53, 11, 'sarjanah', 1),
(54, 9, '4 - 7 th', 2),
(55, 9, '> 8 th', 3),
(56, 10, '3 - 5 bahasa', 2),
(57, 10, '>= 6 bahasa', 3),
(58, 11, 'magister', 2),
(59, 11, 'doctor', 3),
(60, 11, 'profesor', 4),
(62, 12, 'dummy', 1),
(63, 9, 'ojo', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian`
--

CREATE TABLE IF NOT EXISTS `penilaian` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `nidn` varchar(10) NOT NULL,
  `id_kri` int(11) NOT NULL,
  `id_sub_kri` int(11) NOT NULL,
  PRIMARY KEY (`id_nilai`),
  KEY `id_kri` (`id_kri`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data untuk tabel `penilaian`
--

INSERT INTO `penilaian` (`id_nilai`, `nidn`, `id_kri`, `id_sub_kri`) VALUES
(20, 'D10000001', 9, 51),
(21, 'D10000001', 10, 52),
(22, 'D10000001', 11, 53),
(23, 'D10000002', 9, 54),
(24, 'D10000002', 10, 56),
(25, 'D10000002', 11, 60),
(26, 'D10000003', 9, 55),
(27, 'D10000003', 10, 52),
(28, 'D10000003', 11, 58),
(32, 'D10000001', 12, 62),
(33, 'D10000002', 12, 62),
(34, 'D10000003', 12, 62),
(39, 'D10000004', 9, 51),
(40, 'D10000004', 10, 52),
(41, 'D10000004', 11, 53),
(42, 'D10000004', 12, 62);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
